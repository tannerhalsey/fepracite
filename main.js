//change the navbar styles on scroll number determines pixels until navbar shows on scroll
window.addEventListener("scroll", () => {
    document.querySelector('nav').classList.toggle
    ('window-scroll', window.scrollY > 0)
});


// show or hide FAQ questions/anwsers

const faqs = document.querySelectorAll(".faq")

faqs.forEach(faq => {
    faq.addEventListener('click', () =>{
        faq.classList.toggle('open')

        //change icon when opened/closed
        
        const icon = faq.querySelector('.faq__icon i');
        if(icon.className === 'uil uil-plus'){
            icon.className = 'uil uil-minus'
        } else{
            icon.className = 'uil uil-plus'
        }
    })
})

